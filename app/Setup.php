<?php

namespace App;

use App\Middleware\Middleware;
use App\Routes\Routes;
use App\Source\Dependencies;
use Slim\App;
use Slim\Middleware\Session;

class Setup
{
    private $app;

    public function __construct()
    {
        $setting = require __DIR__ . '/Config/settings.php';
        $this->app = new App($setting);
        $this->dependencies();
        $this->middleware();
        $this->routes();
        $this->session();
    }

    public function get()
    {
        return $this->app;
    }

    private function dependencies()
    {
        return new Dependencies($this->app);
    }

    private function middleware()
    {
        return new Middleware($this->app);
    }

    private function routes()
    {
        return new Routes($this->app);
    }

    private function session()
    {
        $this->app->add(new Session([
            'name' => 'slim_session',
            'autorefresh' => true,
            'lifetime' => '1 hour',
        ]));

        $container = $this->app->getContainer();

        $container['session'] = function () {
            return new \SlimSession\Helper;
        };
    }
}
