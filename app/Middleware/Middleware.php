<?php

namespace App\Middleware;

use Tuupola\Middleware\JwtAuthentication;

class Middleware
{
    private $app;
    private $container;

    public function __construct($app)
    {
        $this->app = $app;
        $this->container = $app->getContainer();

        session_start();
        if (!isset($_SESSION['user_id'])) {
            $this->jwtAuthentication();
        }
    }

    public function jwtAuthentication()
    {
        $jwtSettings = $this->container->get('settings')['jwt'];
        $this->app->add(new JwtAuthentication($jwtSettings));
    }
}
