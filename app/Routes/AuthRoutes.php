<?php

namespace App\Routes;

use App\Controllers\AuthController;

class AuthRoutes
{
    public function __construct($app)
    {
        $app->group('/auth', function () {
            $this->post('/register', AuthController::class . ':register');
            $this->post('/login', AuthController::class . ':login');
            $this->post('/session/register', AuthController::class . ':sessionRegister');
            $this->post('/session/login', AuthController::class . ':sessionLogin');
            $this->post('/logout', AuthController::class . ':logout');
        });
    }
}
