<?php

namespace App\Tests;

use App\Tests\Setup\Bootstrap;
use App\Tests\Setup\Helper;
use PHPUnit\Framework\TestCase;

class AuthorizationSessionTest extends TestCase
{
    /** @var Helper */
    private static $helper;

    public static function setUpBeforeClass()
    {
        self::$helper = Bootstrap::getApp();
        Bootstrap::clearDatabase();
    }

    protected function tearDown()
    {
        Bootstrap::clearDatabase();
        session_unset();
    }

    public function testUsersRegistrationWithProperData()
    {
        $user = [
            'email' => "phpunit@email" . date('mdYHis') . ".com",
            'password' => 'phpunit',
            'first_name' => 'phpunit',
            'last_name' => 'phpunit',
            'username' => 'phpunit',
        ];

        $response = self::$helper->apiRequest('post', '/auth/session/register', true, $user);

        $this->assertSame($response['code'], 201);
        $this->assertTrue(isset($response['data']['user']));
        $this->assertTrue(isset($response['data']['auth']));
    }

    public function testUsersRegistrationWithEmailAlreadyExistingInDatabase()
    {
        $alreadyExistedUser = [
            'email' => "phpunit@email" . date('mdYHis') . ".com",
            'password' => 'phpunit',
            'first_name' => 'phpunit',
            'last_name' => 'phpunit',
            'username' => 'phpunit',
        ];

        Bootstrap::createUser($alreadyExistedUser);
        $response = self::$helper->apiRequest('post', '/auth/session/register', false, $alreadyExistedUser);

        $this->assertSame($response['code'], 409);
        $this->assertEquals($response['data']['message'], "The user with such email already exist");
        $this->assertEquals($response['data']['status'], "error");
    }

    public function testUsersRegistrationWithWrongEmailFormat()
    {
        $user = [
            'email' => "phpunitemailcom",
        ];

        $response = self::$helper->apiRequest('post', '/auth/session/register', false, $user);

        $this->assertSame($response['code'], 400);
        $this->assertEquals($response['data']['email'][0], "Email must be valid email");
    }

    public function testUsersRegistrationWithoutAllRequiredFields()
    {
        $user = [
            'email' => "phpunitemailcom",
            'password' => '',
            'first_name' => 'phpunit',
            'last_name' => '',
            'username' => 'phpunit',
        ];

        $response = self::$helper->apiRequest('post', '/auth/session/register', false, $user);

        $this->assertSame($response['code'], 400);
        $this->assertEquals($response['data']['password'][0], "Password must not be empty");
        $this->assertEquals($response['data']['last_name'][0], "Last_name must not be empty");
    }

    public function testUsersLoginWithProperData()
    {
        $user = [
            'email' => "phpunit@email" . date('mdYHis') . ".com",
            'password' => 'phpunit',
        ];
        Bootstrap::createUser($user);

        $response = self::$helper->apiRequest('post', '/auth/session/login', false, [
            'email' => $user['email'],
            'password' => $user['password'],
        ]);

        $this->assertSame($response['code'], 200);
        $this->assertTrue(isset($response['data']['user']));
        $this->assertTrue(isset($response['data']['auth']));
    }

    public function testUsersLoginWithWrongPassword()
    {
        $user = Bootstrap::createUser();

        $response = self::$helper->apiRequest('post', '/auth/session/login', false, [
            'email' => $user['email'],
            'password' => 11111111,
        ]);

        $this->assertSame($response['code'], 401);
        $this->assertFalse(($response['data']['auth']));
        $this->assertEquals($response['data']['status'], "error");
    }

    public function testUsersLoginWithWrongEmail()
    {
        $user = Bootstrap::createUser();

        $response = self::$helper->apiRequest('post', '/auth/session/login', false, [
            'email' => 'a@a.com',
            'password' => $user['password'],
        ]);

        $this->assertSame($response['code'], 401);
        $this->assertFalse(($response['data']['auth']));
        $this->assertEquals($response['data']['status'], "error");
    }
}
